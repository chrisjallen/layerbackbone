/*global define */
define(['jquery', 'underscore', 'backbone'], function () {
    'use strict';
 

  var Size = function(size){
    this.w = size[0];
    this.h = size[1];
    this.a = (this.w || 1)  / (this.h || 1);
  }

  Size.prototype = {
    "w": 0,
    "h": 0,
    "a": 0,
    aspect: function() {
      this.a = this.w / this.h;
    }
  };

  

  var Layer = Backbone.View.extend({

    events: {
    },

    className: 'layer',
    identifier:'base',

    initialize: function() {
      this.subViews = [];
      this.listeners = {};
      this.addListeners();
    },

    addListeners: function() {
      $(window).on('resize', _.debounce(_.bind(this.resizeStarted, this), 500, true));
      $(window).on('resize', _.debounce(_.bind(this.resizeEnded, this), 500));
      $(window).on('resize', _.bind(this.onResize, this));
      $(window).on('scroll', _.debounce(_.bind(this.scrolling, this, 'start'), 800, true));
      $(window).on('scroll', _.debounce(_.bind(this.scrolling, this, 'finish'), 800));
      $(window).on('scroll', _.bind(this.scrolling, this, 'scrolling'));
      $(window).on('orientationchange', _.bind(this.orientation, this, window.orientation));
    },

    removeListeners: function(){
      $(window).off(this.listeners.resizeStarted);
      $(window).off(this.listeners.resizeEnded);
      $(window).off(this.listeners.resize);
      $(window).off(this.listeners.scrollingStarted);
      $(window).off(this.listeners.scrollingEnded);
      $(window).off(this.listeners.scrolling);
      $(window).off(this.listeners.orientation);
    },

    render: function() {
      this.removeChildren();
      this.$el.html('');
      return this;
    },

    removeChildren: function(classIdentifier){
      _.each(this.subViews, function(view){
        if(classIdentifier && view.identifier == classIdentifier) view.leave();
        if(!classIdentifier) view.leave();
      });
    },

    leave: function(){
      this.stopListening();
      this.removeListeners();
      this.remove();
    },

    removeChild: function(view){
      view.leave();
      this.subViews = _.without(this.subViews,view);
    },

    onResize: function() {
      this.trigger('main:resize');
    },

    resizeStarted: function() {
      this.trigger('main:resizeStarted');
    },

    resizeEnded: function() {
      this.trigger('main:resizeFinished');

    },

    scrolling: function(status) {
      switch (status) {
      case 'start':
        this.trigger('main:scrollStarted');
        break;
      case 'scrolling':
        this.trigger('main:scroll');
        break;
      case 'finish':
        this.trigger('main:scrollFinished');
        break;
      }
    },

    orientation: function(beforeOrientation) {},

    addChild:function(view, $parent){
      if($parent){
        $parent.append(view.el);
      }else{
        this.$el.append(view.el);
      }

      if(!this.subViews) this.subViews = [];
      this.subViews.push(view);
    },

    getSize:function(setFromEl){
      if(!this.width){
        this.width = this.$el.width();
        this.height = this.$el.height();
      }
      var s = new Size([this.width, this.height]);
      this.aspect = s.a;
      return s;
    },

    setSize: function(size){
      this.width = size.w;
      this.height = size.h;
      this.aspect = size.a;
      this.$el.css({
          'width': size.w,
          'height': size.h,
      });
      return this;
    },

    isPortrait: function(){
      this.getSize();
      return (this.width / this.height  < 1);
    },

    isLandscape: function(){
      this.getSize();
      return (this.width / this.height  > 1);
    },

    centerPosition: function($parent){
      if(!this.width) this.getSize('hard');
      var s = this.getSize();
      var x = s.w* 0.5;
      var y = s.h * 0.5;
      var $el = $parent || $(window);
      this.$el.css({
          'left': ($el.width()*.5) - x,
          'top':  ($el.height()*.5) - y
      });
    }


  });



  return Layer;
});