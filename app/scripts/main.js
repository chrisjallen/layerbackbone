require.config({
    paths: {
        jquery: '../components/jquery/jquery',
        modernizr: '../components/modernizr/modernizr',
        underscore: '../components/underscore/underscore',
        backbone: '../components/backbone/backbone'
    },
    shim: {
        backbone: {
            deps: ['underscore'],
        }
    }
});

require(['app'], function (App) {
    'use strict';

    // Test Code to check is inserted into dom
    // window.App = App;
    // window.app =new App();
    // $('body').append( window.app.render().el );
    // var childEl = new App();
    // window.app.addChild( childEl );
    
});